import Cookies from 'js-cookie';

import {
  GET_TOKEN_TYPE,
  ADD_TOKEN_TYPE,
  REMOVE_TOKEN_TYPE
} from "./authTypes";

const TOKEN_KEY = 'X-App-Token';

export const getToken = () => dispatch => (
  dispatch({ type: GET_TOKEN_TYPE,  payload: { token: Cookies.get(TOKEN_KEY) }})
);
  
export const addToken = token => dispatch => (
  Promise.all(Cookies.set(TOKEN_KEY, token))
    .then(() => dispatch({ type: ADD_TOKEN_TYPE, payload: { token } }))
);
  
  
export const removeToken = () => dispatch => (
  Promise.all(Cookies.remove(TOKEN_KEY))
    .then(() => dispatch({ type: ADD_TOKEN_TYPE }))
);
