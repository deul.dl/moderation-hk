const initialState = {
  token: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_TOKEN':
    case 'ADD_TOKEN': {
      return { ...state, token: action.payload };
    }
    case 'REMOVE_TOKEN': {
      return { ...state, token: null };
    }
  }
  return state;
};
