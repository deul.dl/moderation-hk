import React from "react";
import { useSelector } from "react-redux";
import { Switch, Route, Redirect, BrowserRouter as Router } from "react-router-dom";

import Login from "./pages/login";
import Main from "./pages//main";
import NewTask  from "./pages/new-task";

const App = () => {
  const auth = useSelector(state => state.auth);

  return (
    <div className="App">
      <Router>
        {!auth.token ? (
          <Switch>
            <Route path="/login" component={Login} />

            <Redirect to="/login" />
          </Switch>
        ) : (
          <Switch>
            <Route path="/new-task" component={NewTask} />
            <Route path="/" component={Main} />     

            <Redirect to="/" />
          </Switch>
        )}
      </Router>
    </div>
  );
}

export default App;
