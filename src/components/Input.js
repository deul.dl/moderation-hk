import React from "react";
import { Form } from "react-bootstrap";

const TextInput = ({ label, meta, input, ...rest }) => {
  return (
    <Form.Group controlId="formBasicEmail">
    {label && <Form.Label>{label}</Form.Label>}
    <Form.Control {...input} {...rest} />
      {meta.touched && meta.error &&
        (
          <Form.Text className="text-muted">
            {meta.error}
          </Form.Text>
        )
      }
    </Form.Group>
  );
}

export default TextInput;
