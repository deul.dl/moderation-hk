import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { Form, Button } from "react-bootstrap";

import TextInput from "../../../components/Input";

const LoginForm = ({ handleSubmit, error }) => (
  <Form onSubmit={handleSubmit}>
    {error && <b>{error}</b>}
    <Field
      component={TextInput}
      label="Phone Number"
      name="phoneNumber"
      type="tel"
    />
    <Field
      component={TextInput}
      label="Password"
      name="password"
      type="password"
    />
    <Button variant="primary" type="submit">
      Login
    </Button>
  </Form>
);

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string
};

export default reduxForm({
  form: "loginForm",
})(LoginForm);
