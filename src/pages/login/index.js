import React from 'react';
import { Container } from 'react-bootstrap';
import { connect } from "react-redux";
import { SubmissionError } from 'redux-form';

import { addToken } from "../../auth/authActions"
import LoginForm from './components/LoginForm';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit({ phoneNumber, password }) {
    console.log(phoneNumber, password);
    if (phoneNumber === '+77056278085' && password === '111111') {
      return this.props.addToken("1");
    }

    throw new SubmissionError({ _error: 'Password or phone number is not right.' });
  }
  
  render() {
    return (
      <Container>
        <LoginForm initialValues={{ phoneNumber: "+77056278085" }} onSubmit={this.handleSubmit} />
      </Container>
    )
  }
}

export default connect(null, {
  addToken,
})(Login);
