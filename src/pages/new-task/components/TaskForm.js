import React from "react";
import { Field, reduxForm } from "redux-form";
import { Form, Button } from "react-bootstrap";

import TextInput from "../../../components/Input";

const TaskForm = ({ handleSubmit }) => (
  <Form onSubmit={handleSubmit}>
    <Field
      component={TextInput}
      label="Name"
      name="name"
      type="text"
    />

    <Button variant="primary" type="submit">
      Create
    </Button>
  </Form>
);

export default reduxForm({
  form: "taskForm",
})(TaskForm);
