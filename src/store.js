import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';

import authReducer from './auth/authReducer';

const rootReducer = combineReducers({
  auth: authReducer,
  form: formReducer
})

export default createStore(rootReducer, applyMiddleware(thunk));
