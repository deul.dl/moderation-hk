const { addBeforeLoader, loaderByName } = require("@craco/craco");

module.exports = {
  webpack: {
    configure: (webpackConfig) => {
      webpackConfig.resolve.extensions.push(".css");
      webpackConfig.resolve.extensions.push(".scss");

      const yamlLoader = {
        test: /\.(css|scss)?$/,
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
          },
          {
            loader: "sass-loader",
          },
        ],
      };

      addBeforeLoader(webpackConfig, loaderByName("file-loader"), yamlLoader);
      return webpackConfig;
    },
  },
};
